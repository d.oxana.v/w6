<?php

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != 'admin' ||
    md5($_SERVER['PHP_AUTH_PW']) != md5('admin')) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}

// Используем метод Double Submit Cookie 
// Создаем токен и помещаем его в куки, а также в input формы.
$token = md5('kubsu' . $_SERVER['PHP_AUTH_USER']);
setcookie('token', $token, time() + 24 * 60 * 60);

// Инициализируем переменные для подключения к базе данных.
$db_user = 'u17310';   
$db_pass = '2610050'; 

// Подключаемся к базе данных на сервере.
$db = new PDO('mysql:host=localhost;dbname=u17310', $db_user, $db_pass, array(
    PDO::ATTR_PERSISTENT => true
));

// Если метод был POST, значит мы нажали на кнопку удаления.
// Пробуем удалить запись из базы данных.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($_POST['token'] === $_COOKIE['token']) {
    try {
      $stmt = $db->prepare('DELETE FROM lloll WHERE login = ?');
      $stmt->execute(array(
        $_POST['remove']
      ));
    } catch (PDOException $e) {
      echo 'Error ' . $e->getMessage();
      exit();
    }
  }
}

try {
    $stmt = $db->query('SELECT * FROM lloll');
    ?>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
        <link rel="stylesheet" href="style.css">
        <title>Web 6</title>
    </head>
    <body>
    <form action="" method="post">
        <input type="hidden" name="token" value="<?php print($token); ?>">
        <div class="table-container">
          <table class="table is-hoverable is-fullwidth">
              <thead>
              <tr>
                  <th>login</th>
                  <th>password</th>
                  <th>name</th>
                  <th>email</th>
                  <th>year</th>
                  <th>sex</th>
                  <th>number of limbs</th>
                  <th>superpowers</th>
                  <th>biography</th>
                  <th>Delete</th>
              </tr>
              </thead>
              <tbody>
              <?php
              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  print('<tr>');
                  foreach ($row as $cell) {
                      print('<td>' . strip_tags($cell) . '</td>');
                  }
                  print('<td><button class="button is-info is-small is-danger is-light" name="remove" type="submit" value="'
                  . strip_tags($row['login'])
                  . '">x</button></td>');
                  print('</tr>');
              }
              ?>
              </tbody>
          </table>
        </div>
    </form>
    </body>
    <?php
} catch (PDOException $e) {
    echo 'Error ' . $e->getMessage();
    exit();
}